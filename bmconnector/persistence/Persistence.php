#!/usr/bin/php
<?php


/**
 * Classe para conexao com o banco de dados
 * atualmente com conexao ao Postgresql
 * @author Fabricio S Costa fabriciojf@gmail.com
 * @since 28/05/2014
 * @version 1.0
 */
class Persistence {

	var $host;
	var $port;
	var $user;
	var $password;
	var $dbname;
	var $connection;

	public function __construct() {
		$this->host = Bootstrap :: read("Database.host");
		$this->port = Bootstrap :: read("Database.port");
		$this->user = Bootstrap :: read("Database.user");
		$this->password = Bootstrap :: read("Database.password");
		$this->dbname = Bootstrap :: read("Database.dbname");
	}

	/**
	 * Executa um sql no banco de dados
	 * @param String $sql a ser executado
	 */
	public function execute($sql) {

		$con = pg_connect("host=" . $this->host . " port=" . $this->port . " dbname=" .
		$this->dbname . " user=" . $this->user . " password=" . $this->password);
		if (!$con) {
			die("Error in connection: " . pg_last_error());
		} else {
			pg_query($sql);
			pg_close($con);
		}
	}

	/**
	 * Realiza uma contagem de registros no banco de dados
	 * @param $sql a ser executado ex:
	 * count("SELECT * FROM usuario WHERE id = 1")
	 */
	public function count($sql) {

		$num_rows = 0;
		$con = pg_connect("host=" . $this->host . " port=" . $this->port . " dbname=" .
		$this->dbname . " user=" . $this->user . " password=" . $this->password);

		if (!$con) {
			die("Error in connection: " . pg_last_error());
		} else {
			$result = pg_query($sql);
			$num_rows = pg_num_rows($result);
			pg_close($con);
		}
		return $num_rows;
	}

	/**
	 * Conecta ao banco de dados 
	 * @param unknown_type $sql
	 */
	public function conect() {
		$this->connection = pg_connect("host=" . $this->host . " port=" . $this->port . " dbname=" .
		$this->dbname . " user=" . $this->user . " password=" . $this->password);
	}

	/**
	 * Executa um sql no banco de dados
	 * @param String $sql a ser executado
	 */
	public function select($sql) {
		return pg_query($sql);
	}

	/**
	 * Executa um sql no banco de dados
	 * @param String $sql a ser executado
	 */
	public function close() {
		pg_close($this->connection);
	}

}