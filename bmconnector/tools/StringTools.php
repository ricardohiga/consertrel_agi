#!/usr/bin/php
<?php

/**
 * Controller de entrada do sistema
*/
class StringTools {
	
	public static function clean($peer) {
		
		$newPeer = strtolower($peer);		
		$newPeer = str_replace('agent/', '', $newPeer);
		$newPeer = str_replace('sip/', '', $newPeer);
		
		return $newPeer;		
	}
	
	public static function validateParam($param) {
		if ($param == null || $param == '' || $param == 'unkown' || $param == 'undefined')
				return 'Nulo';
		return $param;        
	}
}