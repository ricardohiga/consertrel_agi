nano vaki<?php

/**
 * Verifica se o Asterisk está rodando, caso não esteja 
 * notifica o BmTelecom para que seja enviado email ao Suporte 
 * 
 * @author Fabricio Costa fabriciojf@gmail.com
 * @since 2017-08-19
 * 
 * Para agendar a execução no Linux utilize o crontab:
 * 
 * $ crontab -e
 * 
 * 5 * * * * php /var/lib/asterisk/agi-bin/asterisk-is-down.php
 * 
 * O arquivo de log é gerado dentro do arquivo
 * /var/log/asterisk/asterisk-is-down
 * 
 */

$astUrl = 'http://127.0.0.1:8088/asterisk/rawman?action=status';
$cha = curl_init($astUrl);
curl_setopt($cha, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cha, CURLOPT_FOLLOWLOCATION, true);
$astMessage = curl_exec($cha);
$urlInfo = curl_getinfo($cha, CURLINFO_HTTP_CODE);
curl_close($cha);

if ($urlInfo != '200') {
    addLog($astMessage);	
    notify();
    restart();
} 

/**
 * Acessa o BmTelecom para que o suporte seja notificado
 */
function notify() {
    $url= 'http://127.0.0.1/bmtelecom/suporte/asterisk_status';
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_exec($ch);
    curl_close($ch);
}

/**
 * Tenta reiniciar o Asterisk
 */
function restart() {
     shell_exec('service asterisk restart');    
}

/**
 * Adiciona uma linha ao arquivo de log
 */ 
function addLog($outputMessage) {
	$date = date('Y-m-d h-i');
	$message = 'Notificando queda do Asterisk';
	$logFile = '/var/log/asterisk/asterisk-is-down';
			
	shell_exec(sprintf('echo "%s: %s" >> %s',
			$date, $message, $logFile));
	
	shell_exec(sprintf('echo "%s: %s" >> %s',
			$date, $outputMessage, $logFile));
}