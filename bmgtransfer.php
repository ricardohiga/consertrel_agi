#!/usr/bin/php
<?php
require_once('phpagi/phpagi.php');

$agi = new AGI();
$ramal = $argv[1];

$map = array(
     '7001' => 'setor9',
     '7003' => 'setor2',  
     '7005' => 'setor3',  
     '7006' => 'setor12', 
     '7007' => 'setor8',  
     '7009' => 'setor8',
     '7010' => 'setor2',  
     '7013' => 'setor1',  
     '7017' => 'setor8',  
     '7018' => 'setor2',  
     '7020' => 'setor13', 
     '7021' => 'setor13',
     '7024' => 'setor13', 
     '7026' => 'setor13',
     '7027' => 'setor13', 
     '7029' => 'setor10', 
     '7030' => 'setor10', 
     '7032' => 'setor10', 
     '7034' => 'setor11', 
     '7050' => 'setor2',
     '7051' => 'setor2',  
     '7052' => 'setor3',
     '7053' => 'setor3',  
     '7054' => 'setor9',  
     '7055' => 'setor9',  
     '7056' => 'setor1',  
     '7057' => 'setor10',
     '7058' => 'setor10'
);

$setor = array_key_exists($ramal, $map) ? $map[$ramal] : 'NO_MAPPED';
$agi->set_variable("TSETOR", $setor);

exit(0);

?>
