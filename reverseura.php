#!/usr/bin/php
<?php
/**
 * Forma de chamar o arquivo reverseura
 *
 * ...
 * exten => h,n,AGI(reverseura.php,${PHONE_NUMBER},${EXTEN},${CAMPAIGN_ID})
 * ...
 *
 * @author Fabricio S Costa
 * @version 3.2.2
 * @since 2018/02/22
 *
 * [ura-reversa]
 *
 * exten => _X.,1,Answer()
 * exten => _X.,n,Background(${SOUND_VOICE})
 * exten => _X.,n,Set(TIMEOUT(response)=5)
 * exten => _X.,n,Set(TIMEOUT(digit)=5)
 * exten => _X.,n,WaitExten()
 *
 * exten => _X,1,Answer()
 * exten => _X,n,NoOp(teclou ${EXTEN})
 * exten => _X,n,AGI(reverseura.php,${PHONE_NUMBER},${EXTEN},${CAMPAIGN_ID})
 */

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/tools/StringTools.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$confs = new Bootstrap();

$agi->noop('====> Callerid: ' . $agi->request['agi_callerid']);
$agi->noop('====> Uniqueid: ' . $agi->request['agi_uniqueid']);
$agi->noop('====> calleridname: ' . $agi->request['agi_calleridname']);
$agi->noop('====> Arg1: ' . $agi->request['agi_arg_1']);
$agi->noop('====> Arg2: ' . $agi->request['agi_arg_2']);

/**
 * $argv[1] = $phone_number
 * $argv[2] = $reponse
 * $argv[3] = $campaign_id
 */
$url = sprintf("http://%s/%s/connector/ura_response/%s/%s/%s",
    $confs->read('System.host'),
    $confs->read('System.name'),
    $argv[1],
    $argv[2],
    $argv[3]
);

$agi->noop('====> URL: ' . $url);

/**
 * Resposta do system via CUrl
 */
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

exit();
?>
