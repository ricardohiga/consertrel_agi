#!/usr/bin/php -q
<?php

set_time_limit(30);
error_reporting(E_ALL);

require_once 'phpagi-2.20/phpagi.php';
require_once 'pdo/pdo.php';

$agi   = new AGI();
$pdo   = new MyPDO();

$year = date("Y");

$channel        = $agi->get_variable("CHANNEL")['data'];
$from           = $agi->get_variable("KSmsFrom")['data'];
$datetime       = $agi->get_variable("KSmsDate")['data'];
$size           = $agi->get_variable("KSmsSize")['data'];
$body           = $agi->get_variable("KSmsBody")['data'];
$type           = $agi->get_variable("KSmsType")['data'];

list($date, $time) = explode(",", $datetime);
$date = substr($year, 0,-2).str_replace("/", "-", $date);
list($time,$rest) = explode("-", $time);

list($channel,$rest) = explode("-", $channel);
$channel = str_replace("Khomp_SMS/", "", $channel);

$agi->Verbose("Mensagem SMS recebida");
$agi->Verbose("Inserindo informacoes no banco");
$agi->Verbose("Canal: ".$channel." | Tipo: ".$type." | De:".$from." | Data:".$date." ".$time." | Tamanho:".$size." | Mensagem:".$body." ");

$query = ("INSERT INTO `sms_received`(`datetime`, `channel`, `type`, `size`, `from`, `body`) VALUES ('".$date." ".$time."','".$channel."','".$type."','".$size."','".$from."','".$body."')");
$pdo->Insert($query);

$agi->Verbose("Encerrando AGI");

